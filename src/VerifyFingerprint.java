
import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.KeyStroke;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ScheduledExecutorService;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import sk.mimac.fingerprint.FingerprintException;
import sk.mimac.fingerprint.FingerprintSensor;
import sk.mimac.fingerprint.adafruit.AdafruitSensor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fdenimar
 */
public class VerifyFingerprint extends javax.swing.JFrame {

    
    DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();  
    
    String stringDate = dateFormat.format(now);
    
    //Fingerprint
    byte [] fingerprintTemplate;
    Employee[] employeesWithFingerprint = new Employee[512];
    String selectedEmployeeID = "0";
    Boolean onCapture = false;
    FingerprintSensor sensor = null;
    
    //To be loaded from the config files
    String accountID = "1";
    String accountName = "Account Name";
    String COMport = "/dev/ttyUSB0";
    String BASE_URL = "https://wbcs.ippei.ph";
    String DB_HOST = "128.199.164.151";
    String DB_PORT = "3306";
    String DB_NAME = "wbcs";
    String DB_USER = "wbcskiosk";
    String DB_PASSWORD = "ippei2019";
    
    //Static status bar background colors.
    Color BACKGROUND_RED = new Color(189,38,15);
    Color BACKGROUND_ORANGE = new Color(224,143,20);
    Color BACKGROUND_GREEN = new Color(15,189,18);
    
    String OS = OSValidator.getOSInfo();
    Connection conn = null;
    
    public Thread fingerThread = null;
    public Runnable getEmployeesRunnable = null;
    
    public Runnable checkInternetAndBackgroundSaveThread = new Runnable() {
        public void run() {
            if (checkInternetConnection() == true) {
                updateStatusBar("Internet connection detected. Checking for offline attendance logs...", BACKGROUND_GREEN);
                saveAttendanceFromCSV();
            } else {
                updateStatusBar("No Internet connection! Attendances will be saved offline then will be synced once back online.", BACKGROUND_RED);
            }
        }
    };
    
    /**
     * Creates new form EnrollFingerprint
     */
    public VerifyFingerprint() {
        initComponents();
    
        
        //Loading props from file
        try {
            Properties prop=new Properties(); 
            FileInputStream ip;
            ip = new FileInputStream("config.properties");
            prop.load(ip);
            
            accountID = prop.getProperty("account_id");
            accountName = prop.getProperty("account_name");
            COMport = prop.getProperty("com_port");
            BASE_URL = prop.getProperty("api_url");
            DB_HOST = prop.getProperty("db_host");
            DB_PORT = prop.getProperty("db_port");
            DB_NAME = prop.getProperty("db_name");
            DB_USER = prop.getProperty("db_user");
            DB_PASSWORD = prop.getProperty("db_password");
             
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "config.properties not found!", "Error locating config file", JOptionPane.ERROR_MESSAGE);
        } catch (IOException f) {
            JOptionPane.showMessageDialog(null, f.getMessage(), "Error getting config file", JOptionPane.ERROR_MESSAGE);
        }
        
        
        //Start capture thread
        this.fingerThread = new Thread() {
            public void run() {
                
                while (!Thread.interrupted()){
                    if (sensor == null) {
                        initializeCapture();
                    }
                }   
                
                disconnectSensor();
            }
        };
        
        this.getEmployeesRunnable = new Runnable() {
            public void run() {                
                PreparedStatement st;
                ResultSet rs;
        
                try
                {
                    st = cn().prepareStatement("select * from `employees` "
                            + "where exists (select * from `employee_deployments` "
                            + "where `employees`.`id` = `employee_deployments`.`employee_id` and `status` in ('Regular', 'Probationary', 'Seasonal', 'Reliever', 'Trainee')"
                            + " and `account_id` = ? and `hire_date_actual` <= ? and (`resign_date_actual` >= ? or `resign_date_actual` is null))"
                            + " and `employees`.`deleted_at` is null order by `status` asc, `last_name` asc");
                    st.setString(1, accountID);
                    st.setString(2, stringDate);
                    st.setString(3, stringDate);
                    rs = st.executeQuery();
                    int index = 0;
                    
                    //Delete existing employee data from CSV.
                    File file = new File("denemployees.csv");
                    if(file.delete()) { 
                       System.out.println("File deleted successfully"); 
                    } 
                    while (rs.next()) {
                        try {
                            if (rs.getBytes("fingerprint") != null) {
                                System.out.println(rs.getString("last_name")+" "+rs.getString("first_name"));
                                
                                //Save fingerprint data to sensor
                                sensor.saveModel(rs.getBytes("fingerprint"), index);
                                
                                //Save employee data to CSV
                                saveOfflineEmployee(rs.getString("id"), rs.getString("first_name"), rs.getString("last_name"), rs.getString("account_id"), rs.getString("photo"));
                                
                                //Save employee data to Array
                                employeesWithFingerprint[index] = new Employee();
                                employeesWithFingerprint[index].id = rs.getString("id");
                                employeesWithFingerprint[index].last_name = rs.getString("last_name");
                                employeesWithFingerprint[index].first_name = rs.getString("first_name");
                                employeesWithFingerprint[index].account_id = rs.getString("account_id");
                                employeesWithFingerprint[index].picture_path = rs.getString("photo");
                                index++;
                            }
                        } catch (FingerprintException ex) {
                            System.out.println("Error saving fingerprint to sensor! "+ex.getMessage()+rs.getString("last_name")+" "+rs.getString("first_name"));
                        }
                    }

                    updateStatusBar("Ready. Loaded "+(index)+" fingerprint data to sensor!", BACKGROUND_GREEN);
                    startCapturing();
                }
                catch (Exception e)
                {
                    updateStatusBar("Error getting fingerprint data. Loaded saved data from the sensor.", BACKGROUND_ORANGE);
                }
            }
        };
        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox();
        jPanel1 = new javax.swing.JPanel();
        Logo = new javax.swing.JLabel();
        employeepic = new javax.swing.JLabel();
        employeeNameTxt = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        employeeTimeTxt = new javax.swing.JLabel();
        accountNameTxt = new javax.swing.JLabel();
        lblStatus = new javax.swing.JLabel();

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(480, 320));
        setMinimumSize(new java.awt.Dimension(480, 320));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel1.setPreferredSize(new java.awt.Dimension(320, 240));
        jPanel1.addContainerListener(new java.awt.event.ContainerAdapter() {
            public void componentAdded(java.awt.event.ContainerEvent evt) {
                jPanel1ComponentAdded(evt);
            }
        });

        Logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ippei_logo.png"))); // NOI18N
        Logo.setToolTipText("");

        employeeNameTxt.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        employeeNameTxt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        employeeNameTxt.setText("Place your finger into the scanner for attendance.");

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel1.setText("ATTENDANCE");

        employeeTimeTxt.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        employeeTimeTxt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        employeeTimeTxt.setText("Welcome!");

        accountNameTxt.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        accountNameTxt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        accountNameTxt.setText("Account Name");

        lblStatus.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        lblStatus.setText("  Please wait...");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(employeeNameTxt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 480, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(employeepic, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(Logo, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(accountNameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(21, 21, 21))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(employeeTimeTxt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(lblStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Logo)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(accountNameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(employeepic, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(employeeNameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(employeeTimeTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(31, 31, 31))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 480, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jPanel1ComponentAdded(java.awt.event.ContainerEvent evt) {//GEN-FIRST:event_jPanel1ComponentAdded
        // TODO add your handling code here:
        addKeyBind(jPanel1, "F10");
    }//GEN-LAST:event_jPanel1ComponentAdded
    
    protected void connectSensor() {
        try {
            sensor = new AdafruitSensor(COMport);
            sensor.connect();
        } catch (FingerprintException ex) {
            Logger.getLogger(VerifyFingerprint.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    protected void disconnectSensor() {
        try {
            sensor.close();
        } catch (IOException e) {
            System.out.println(e);
        }
    }
    
    protected Connection cn() 
    {
	try 
        { 
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://"+DB_HOST+":"+DB_PORT+"/"+DB_NAME+"", DB_USER, DB_PASSWORD);
	} 
        catch(Exception e) 
        { 
            System.out.println(e); 
        }
	return conn;
    }
    
    protected void initializeCapture() {
        checkInternetConnection(); //Just to update the time
        connectSensor();
        accountNameTxt.setText(accountName);
        
        //Check offline employee data if existing.
        File csvEmployeesFile = new File("denemployees.csv");
        boolean isFileEmpty = csvEmployeesFile.length() == 0;
        
        if (isFileEmpty) {
            //Tell the user to update the file first by pressing F10.
            updateStatusBar("Employee data not found, please press F10 to download data and initialize for the first time.", BACKGROUND_RED);
        } else {
            //Load the CSV and start capturing.
            try {
                
                // create BufferedReader and read data from csv
                BufferedReader csvReader = new BufferedReader(new FileReader("denemployees.csv"));
                String row;
                int index = 0;
                while ((row = csvReader.readLine()) != null) {
                    /**
                     * data[0] - employee_id
                     * data[1] - first_name
                     * data[2] - last_name
                     * data[3] - account_id
                     * data[4] - photo
                     */
                    String[] data = row.split(",");
                    
                    employeesWithFingerprint[index] = new Employee();
                    employeesWithFingerprint[index].id = data[0];
                    employeesWithFingerprint[index].first_name = data[1];
                    employeesWithFingerprint[index].last_name = data[2];
                    employeesWithFingerprint[index].account_id = data[3];
                    employeesWithFingerprint[index].picture_path = data[4];
                    
                    index++;
                }
                
                updateStatusBar("Ready.", BACKGROUND_GREEN);
                csvReader.close();
                
                startCapturing();
            } catch (Exception ex) {
                Logger.getLogger(VerifyFingerprint.class.getName()).log(Level.SEVERE, null, ex);
                updateStatusBar("Cannot load employee data from file, please update again by pressing F10.", BACKGROUND_RED);
            }
        }       
    }
    
    Action refreshEmployees = new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
          getEmployees();
        }
      };

    private static final String REFRESH_EMPLOYEES = "refreshEmployees";

    private void addKeyBind(JComponent contentPane, String key) {
      InputMap iMap = contentPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
      ActionMap aMap = contentPane.getActionMap();
      iMap.put(KeyStroke.getKeyStroke(key), REFRESH_EMPLOYEES);
      aMap.put(REFRESH_EMPLOYEES, refreshEmployees);
    }
    
    protected void getEmployees() {
        //Stops capturing for fingerprints.
        onCapture = false;

        updateStatusBar("Getting fingerprint data, please wait...", BACKGROUND_ORANGE);

        try {
            Thread.sleep(100);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Start a new thread to fetch fingerprint data in background.
        Thread getEmployeesThread = new Thread(getEmployeesRunnable);
        getEmployeesThread.start();
    }
    
    protected boolean checkInternetConnection() {
        JSONParser parser = new JSONParser();
        
        try {
            URL url = new URL(BASE_URL+"/api/current-date-time");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            
            String output;
            String stringData = new String();
            
            while ((output = br.readLine()) != null) {
                stringData += output;
            }
            
            Object obj = parser.parse(stringData);
            JSONObject currentDateTime = (JSONObject) obj;
            
            System.out.println(currentDateTime.get("datetime"));
            stringDate = (String) currentDateTime.get("date");

            conn.disconnect();
            
            return true;
            
        } catch (Exception e) {
            return false;
        }
    }

    protected void startCapturing()
    {
        try {
            onCapture = true;
            while(onCapture) {
                if (sensor.hasFingerprint()) {
                    Integer fingerId = sensor.searchFingerprint();
                    System.out.println(fingerId);
                    if (fingerId != null) { // Already known fingerprint 
                        System.out.println("Scanned fingerprint with ID " + fingerId);
                        try {
                            selectedEmployeeID = employeesWithFingerprint[fingerId].id;
                            employeeNameTxt.setText(employeesWithFingerprint[fingerId].last_name+", "+employeesWithFingerprint[fingerId].first_name);
                            getEmployeePicture(employeesWithFingerprint[fingerId].picture_path);
                        } catch (NullPointerException n) {
                            
                        }
                        
                        updateStatusBar("Saving, please wait...", BACKGROUND_ORANGE);
                        saveAttendance();
                    }
                }

            }
        } catch (FingerprintException e) {
            updateStatusBar("Fingerprint Error. Please restart the application.", BACKGROUND_RED);
            disconnectSensor();
        }
    }
    
    
    public void saveAttendance() {
        if (checkInternetConnection() == true) {
            JSONParser parser = new JSONParser();

            try {
                System.out.println(selectedEmployeeID);
                StringBuilder parameters = new StringBuilder();

                parameters.append("employee_id=");
                parameters.append(URLEncoder.encode(new String(selectedEmployeeID), "UTF-8"));
                parameters.append("&secret=");
                parameters.append(URLEncoder.encode(new String("wbc$.secret"), "UTF-8"));
                parameters.append("&date=");
                parameters.append(URLEncoder.encode(new String(dateFormat.format(now)), "UTF-8"));
                parameters.append("&time=");
                parameters.append(URLEncoder.encode(new String(timeFormat.format(now)), "UTF-8"));
                parameters.append("&account_id=");
                parameters.append(URLEncoder.encode(new String(accountID), "UTF-8"));

                URL url = new URL(BASE_URL+"/api/time-punch/save");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("charset","UTF-8");
                conn.setRequestProperty("Content-Length",Integer.toString(parameters.toString().getBytes().length));

                DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
                wr.writeBytes(parameters.toString());
                wr.flush();
                wr.close();

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode() + conn.getResponseMessage());
                }

                InputStreamReader in = new InputStreamReader(conn.getInputStream());
                BufferedReader br = new BufferedReader(in);

                String output;
                String stringData = new String();

                while ((output = br.readLine()) != null) {
                    stringData += output;
                }

                Object obj = parser.parse(stringData);
                JSONObject timePunch = (JSONObject) obj;

                employeeTimeTxt.setText("Clocked at "+timePunch.get("date").toString()+", "+timePunch.get("time").toString());
                saveOfflineLogs(selectedEmployeeID, timePunch.get("date").toString(), timePunch.get("time").toString(), accountID, "Online");
                
                updateStatusBar("Saved attendance! Ready.", BACKGROUND_GREEN);
                conn.disconnect();

            } catch (Exception e) {
                updateStatusBar("Internet connection problem. Attendance will be saved locally...", BACKGROUND_ORANGE);
                employeeTimeTxt.setText("Clocked at "+dateFormat.format(now)+", "+timeFormat.format(now));
                saveOfflineAttendance(selectedEmployeeID, dateFormat.format(now).toString(), timeFormat.format(now).toString(), accountID);
                saveOfflineLogs(selectedEmployeeID, dateFormat.format(now).toString(), timeFormat.format(now).toString(), accountID, "Offline");
            }
        } else {
            updateStatusBar("Internet connection problem. Attendance will be saved locally...", BACKGROUND_ORANGE);
            employeeTimeTxt.setText("Clocked at "+dateFormat.format(now)+", "+timeFormat.format(now));
            saveOfflineAttendance(selectedEmployeeID, dateFormat.format(now).toString(), timeFormat.format(now).toString(), accountID);
            saveOfflineLogs(selectedEmployeeID, dateFormat.format(now).toString(), timeFormat.format(now).toString(), accountID, "Offline");
        }
    }

    public void saveOfflineAttendance(String employee_id, String date, String time, String account_id) {
        try {
            FileWriter csvWriter = new FileWriter("denlogs.csv", true);
            csvWriter.append(employee_id);
            csvWriter.append(",");
            csvWriter.append(date);
            csvWriter.append(",");
            csvWriter.append(time);
            csvWriter.append(",");
            csvWriter.append(account_id);
            csvWriter.append("\n");
            
            csvWriter.flush();
            csvWriter.close();
            
            System.out.println("Offline attendance for Employee "+employee_id+" saved.");
        } catch (IOException ex) {
            Logger.getLogger(VerifyFingerprint.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void saveOfflineLogs(String employee_id, String date, String time, String account_id, String status) {
        try {
            FileWriter csvWriter = new FileWriter("logs.csv", true);
            csvWriter.append(employee_id);
            csvWriter.append(",");
            csvWriter.append(date);
            csvWriter.append(",");
            csvWriter.append(time);
            csvWriter.append(",");
            csvWriter.append(account_id);
            csvWriter.append(",");
            csvWriter.append(status);
            csvWriter.append("\n");
            
            csvWriter.flush();
            csvWriter.close();
            
            System.out.println("Offline attendance for Employee "+employee_id+" saved.");
        } catch (IOException ex) {
            Logger.getLogger(VerifyFingerprint.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void saveOfflineEmployee(String employee_id, String first_name, String last_name, String account_id, String photo) {
        try {
            FileWriter csvWriter = new FileWriter("denemployees.csv", true);
            csvWriter.append(employee_id);
            csvWriter.append(",");
            csvWriter.append(first_name);
            csvWriter.append(",");
            csvWriter.append(last_name);
            csvWriter.append(",");
            csvWriter.append(account_id);
            csvWriter.append(",");
            csvWriter.append(photo);
            csvWriter.append("\n");
            
            csvWriter.flush(); //Writes the line into file.
            csvWriter.close();
            
            System.out.println("Offline data for Employee "+employee_id+" "+last_name+", "+first_name+" saved.");
        } catch (IOException ex) {
            Logger.getLogger(VerifyFingerprint.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateStatusBar(String message, Color background) {
        lblStatus.setText("  "+message);
        lblStatus.setOpaque(true);
        lblStatus.setForeground(Color.white);
        lblStatus.setBackground(background);
    }
    
    public void saveAttendanceFromCSV() {
        File csvFile = new File("denlogs.csv");
        if (csvFile.isFile()) {
            try {
                List<String> attendanceNotSaved = new ArrayList();
                
                // create BufferedReader and read data from csv
                BufferedReader csvReader = new BufferedReader(new FileReader("denlogs.csv"));
                String row;
                int counter = 0;
                int countNotSaved = 0;
                while ((row = csvReader.readLine()) != null) {
                    String message = "Offline attendance logs detected. Will now attempt to send it to the server.";
                    updateStatusBar(message, new Color(224, 143, 20));
                
                    counter++;                
                    String[] data = row.split(",");
                    
                    StringBuilder parameters = new StringBuilder();

                    parameters.append("employee_id=");
                    parameters.append(URLEncoder.encode(new String(data[0]), "UTF-8"));
                    parameters.append("&secret=");
                    parameters.append(URLEncoder.encode(new String("wbc$.secret"), "UTF-8"));
                    parameters.append("&date=");
                    parameters.append(URLEncoder.encode(new String(data[1]), "UTF-8"));
                    parameters.append("&time=");
                    parameters.append(URLEncoder.encode(new String(data[2]), "UTF-8"));
                    parameters.append("&account_id=");
                    parameters.append(URLEncoder.encode(new String(data[3]), "UTF-8"));

                    URL url = new URL(BASE_URL+"/api/time-punch/save");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setDoOutput(true);
                    conn.setDoInput(true);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    conn.setRequestProperty("charset","UTF-8");
                    conn.setRequestProperty("Content-Length",Integer.toString(parameters.toString().getBytes().length));

                    DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
                    wr.writeBytes(parameters.toString());
                    wr.flush();
                    wr.close();

                    if (conn.getResponseCode() != 200) {
                        
                        attendanceNotSaved.add(row);
                        counter--;
                        countNotSaved++;
                        
                    }
                }
                csvReader.close();
                
                updateStatusBar(counter+" offline attendance logs are now sent to the server.", BACKGROUND_GREEN);
                
                if (countNotSaved > 0) {
                    //Add to file (clear the current logs file)
                    FileWriter fw = new FileWriter("denlogs.csv", true);
                    for (String line : attendanceNotSaved) {
                        fw.append(line);
                    }
                    fw.flush();
                    fw.close();
                } else {
                    File file = new File("denlogs.csv");
                    if(file.delete()) 
                    { 
                        System.out.println("File deleted successfully"); 
                    } 
                }
        
            } catch (Exception ex) {
                Logger.getLogger(VerifyFingerprint.class.getName()).log(Level.SEVERE, null, ex);
                updateStatusBar("Cannot send offline attendance to the server... will attempt again in a few moments.", new Color(224, 143, 20));
            }
        } else {
            updateStatusBar("Ready", new Color(15,189,18));
        }
    }
    
    public void getEmployeePicture(String pic_path) 
    {
        System.out.println(pic_path);
        try {       
            BufferedImage image;
            if (pic_path == null || checkInternetConnection() == false) {
                image = ImageIO.read(getClass().getClassLoader().getResource("employee_icon.png"));
            } else {
                image = ImageIO.read(URI.create(BASE_URL+"/"+pic_path).toURL());
            }

            employeepic.setIcon(new ImageIcon(
                    image.getScaledInstance(employeepic.getWidth(), employeepic.getHeight(), Image.SCALE_DEFAULT)));
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VerifyFingerprint.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VerifyFingerprint.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VerifyFingerprint.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VerifyFingerprint.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                VerifyFingerprint vf = new VerifyFingerprint();
                
                vf.dispose();   
                vf.setUndecorated(true);
                vf.setExtendedState(JFrame.MAXIMIZED_BOTH); 
                vf.setVisible(true);
                
                vf.fingerThread.start();
                ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
                service.scheduleAtFixedRate(vf.checkInternetAndBackgroundSaveThread, 60, 180, TimeUnit.SECONDS);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Logo;
    private javax.swing.JLabel accountNameTxt;
    private javax.swing.JLabel employeeNameTxt;
    private javax.swing.JLabel employeeTimeTxt;
    private javax.swing.JLabel employeepic;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblStatus;
    // End of variables declaration//GEN-END:variables
}
